
from typing import List
import unittest
from unittest import mock,TestCase
class Chat:
    def __init__(self,nom:str,couleur:str) -> None:
        self.nom = nom
        self.couleur = couleur
    def is_roux(self) -> bool:
        return True if self.couleur == 'roux' else False
    
class ChatDAO:
    @staticmethod
    def find_all_chats() -> List[Chat]:
        pass
    

class ChatService:
    def compter_nombre_chats_roux(self) -> int:
        chats= ChatDAO.find_all_chats()
        chats = list(filter(lambda chat: chat.is_roux(), chats))
        return len(chats)
    
class TestCompterChat(TestCase):

    def test_compterNombreChatsRoux_ShouldBeFine(self):
        with mock.patch.object(ChatDAO, 'find_all_chats', return_value=[Chat("felix","roux"),Chat("felix2","gris"),Chat("felix3","roux")]):
            chatService = ChatService()
            self.assertEqual(chatService.compter_nombre_chats_roux(),2)

if __name__ == "__main__":
    unittest.main()
