import unittest

class MyTest(unittest.TestCase):

    def test_split(self):
        s = 'hello world'
        # va renvoyer une exception car 1 n'est pas un str
        with self.assertRaises(TypeError):
            s.split(1)

if __name__ == '__main__':
    unittest.main()